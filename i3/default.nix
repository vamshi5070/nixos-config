{config,lib}:{
xsession.windowManager.i3 = {
enable = true;
config = {
terminal = "alacritty";
startup = [
{command = "xrandr -s 1920x1080";}
{command = "alacritty";}
];
};
};
}
