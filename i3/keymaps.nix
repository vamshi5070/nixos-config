{config,lib}:
let 
modifier = config.xsession.windowManager.i3.config.modifier;
in lib.mkOptionDefault {
"${modifier}+Shift+c"="kill";
}
