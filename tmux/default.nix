{pkgs,...}:{
programs.tmux = {
  enable = true;
  keyMode = "vi";
  terminal = "tmux-256color";
  plugins = with pkgs; [
    tmuxPlugins.better-mouse-mode
    # tmuxPlugins.nord
    {
      plugin = tmuxPlugins.catppuccin;
      extraConfig = '' 
      set -g @catppuccin_flavour 'frappe'
      set -g @catppuccin_window_tabs_enabled on
      set -g @catppuccin_date_time "%H:%M"
    '';
    }
  ];
  #mouse = true;
  prefix = "C-b";
  escapeTime = 0;
  baseIndex = 1;
  customPaneNavigationAndResize = true;
  # extraConfig = ''
  #   bind-key -T copy-mode-vi 'v' send -X begin-selection
  #   bind-key -T copy-mode-vi 'y' send-keys -X copy-pipe-and-cancel 'xclip -sel clip -i'
  #   set -g status-right ""
  #   setw -g status-style 'bg=colour0 fg=colour7'
  #   setw -g window-status-current-format '[#P:#W*] '
  #   '';
  };
}
