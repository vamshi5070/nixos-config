{...}: {
  programs = {
    nixvim = {
      keymaps = [
        {
          action = "<cmd>ToggleTerm<CR>";
          key = "<F4>";
	  # <leader>tm";
          mode = "n";
          options = {
            silent = true;
            desc = "Toggle terminal";
          };
        }
      ];
      plugins = {
        toggleterm = {
          enable = true;
          settings = {
            # direction = "float";
            closeOnExit = false;
          };
        };
        which-key = {
          registrations = {
            "<F4>" = "Toggle terminal";
          };
        };
      };
    };
  };
}
