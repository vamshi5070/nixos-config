{ pkgs, nixvim, ... }: {
home.packages=  with pkgs;[fd];
  imports = [
    ./telescope
    ./ui/alpha.nix
    # ./ui/staline.nix
    ./ui/lualine.nix
    # ./colorscheme/colorscheme.nix
    # ./ui/heirline.nix
    ./ui/noice.nix
    ./treesitter/treesitter.nix
    ./treesitter/treesitter-textobjects.nix
    ./utils/hardtime.nix
    ./utils/harpoon.nix
    ./utils/flash.nix
    ./utils/mini.nix
    ./ui/toggleterm.nix
    # ./utils/ufo.nix
    # ./ui/precognition.nix
  ];

  programs.nixvim = {
    enable = true;
    globals.mapleader = " ";
    # extraPlugins = with pkgs.vimPlugins; [ heirline-nvim ];
    extraConfigLua = ''
      --Neovide
      if vim.g.neovide then
      --Neovide options
      vim.g.neovide_transparency = 0.9
      vim.g.neovide_padding_top  = 10
      vim.g.neovide_padding_left = 10
      end
    '';
    # extraConfigLua = builtins.readFile ./statusBar.lua;
    colorschemes.catppuccin = {
      enable = true;
      settings = { flavour = "mocha"; };
    };
    keymaps = [
      {
        mode = "n";
        key = "<leader>co";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>e /home/vamshi/nixos-config/nixvim/default.nix<CR>";
      }

      {
        mode = "n";
        key = "<leader>fd";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>e .<CR>";
      }
      {
        mode = "n";
        key = "<leader>fs";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>w<CR>";
      }
      {
        mode = "n";
        key = "<leader>wv";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>vs<CR>";

      }
      {
        mode = "n";
        key = "<leader>wc";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>q<CR>";

      }

      {
        mode = "n";
        key = "<leader>ws";
        options.silent = true;
        # action = "<cmd>!make<CR>";
        action = "<cmd>sp<CR>";

      }
    ];
    plugins = {
      #      treesitter = {
        # enable = true;
        # folding = false;
        #      };
        which-key.enable = true;
        indent-blankline.enable = true;
        # lualine.enable = true;			
        # telescope = {
          #   enable = true;
          #   keymaps = {
            #     "<leader>/" = {
              #       action = "live_grep";
              #       options = {
                # 	desc = "Grep (root dir)";
                #       };
                #     };
                #   };
                # };
                oil.enable = true;
                gitsigns.enable = true;
                fugitive.enable = true;
    };
    opts = {
      number = true;
      relativenumber = true;
      shiftwidth = 2;
      cursorline = true;
      autochdir = true;
    };
  };

  home.file.".config/neovide/config.toml".text = ''
    [font]
    normal = ["monospace"]
    size = 9
  '';
}
