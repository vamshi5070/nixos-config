{...}:{
 programs.kitty = {
  enable = true;
  font = {
    name = "Monospace";
    size = 14;
 };
  theme = "Dracula";

# settings = {
  #  font = "Iosevka Term SS05 12";
#  font = {
#  normal = {
#  family = "Fira Code";
  
#};
#size = 7.0;
#};

#};
 };
}
