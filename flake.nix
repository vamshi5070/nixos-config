{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # neovim-nightly-overlay = {
    #   url = "github:nix-community/neovim-nightly-overlay";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    # nix-doom-emacs-unstraightened.url =
    #   "github:marienz/nix-doom-emacs-unstraightened";
    # nix-doom-emacs-unstraightened.inputs.nixpkgs.follows = "";
    #
    # emacs-overlay = {url = "github:nix-community/emacs-overlay";};
    # vscode-server.url = "github:nix-community/nixos-vscode-server";
    # nixvim = {
    #   url = "github:nix-community/nixvim";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
  };
  outputs = inputs @ {nixpkgs, ...}:
  # outputs = inputs@{ self, nixpkgs, nix-doom-emacs-unstraightened, nixvim
  #   , vscode-server, ... }:
  let
    system = "x86_64-linux";
    # system = "aarch64-linux";
    myPkgs = import nixpkgs {
      inherit system;
      config = {
        allowUnfree = true;
        allowUnsupportedSystem = true;
      };
    };
  in {
    homeConfigurations = {
      nixos = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = myPkgs; # nixpkgs.legacyPackages.${system};
        modules = [
          {
            programs.htop.enable = true;
            services.polybar = {
              enable = true;
              script = "polybar bar &";
              config = {
                "bar/top" = {
                  monitor = "\${env:MONITOR:eDP-1}";
                  width = "100%";
                  height = "3%";
                  radius = 0;
                  modules-center = "date";
                };

                "module/date" = {
                  type = "internal/date";
                  internal = 5;
                  date = "%d.%m.%y";
                  time = "%H:%M";
                  label = "%time%  %date%";
                };
              };
            };
            # programs.ghostty.enable = true;
            # programs.doom-emacs.enable = true;
          }
          ({pkgs, ...}: {
            # fonts.packages = [
            # ...
            # (pkgs.nerdfonts.override { fonts = [ "0xproto" "DroidSansMono" ]; })
            #   (pkgs.nerdfonts.override { fonts = [ "Agave" ]; })
            # ];

            home.packages = [
              pkgs.brightnessctl
              pkgs.lemonbar
              pkgs.tint2
              pkgs.dzen2
              # (pkgs.nerdfonts.override { fonts = [ "Agave" ]; })
              # pkgs.zed-editor
              # pkgs.silver-searcher
              # pkgs.neovide
              # inputs.nixvim.homeManagerModules.nixvim
            ];
          })

          # ./tmux
          # ./kitty
          # nix-doom-emacs-unstraightened.hmModule
          # nixvim.homeManagerModules.nixvim
          # ./nixvim
          ./direnv
          ./git
          #        ./emacs
          # ./autorandr
          # ./i3
          ./alacritty
          # ./picom
          #            ./zoxide
          #            ./pls
          #            ./bash
          #            ./kakoune
          # ./hyprland
          # ./carapace
          {
            # programs.foot = {
            # };

            # programs.
            programs = {
              foot = {
                enable = true;
                settings = {main = {font = "AgaveNerdFontMono:size=19";};};
              };
              bash.enable = true;
              starship = {
                enable = true;
                enableBashIntegration = true;
              };
            };

            # nixpkgs.overlays = with inputs; [ emacs-overlay.overlay ];
            programs.home-manager.enable = true;
            home = {
              username = "vamshi";
              homeDirectory = "/home/vamshi";
              stateVersion = "24.11";
            };
          }
        ];
      };
    };
  };
}
