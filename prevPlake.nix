{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      # inputs.nixpkgs.follows = "nixpkgs";
    };
    #inputs.
    #    vscode-server.url = "github:msteen/nixos-vscode-server";
    vscode-server.url = "github:nix-community/nixos-vscode-server";
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Tricked out nvim
    #     pwnvim.url = "github:zmre/pwnvim";

    #     vscode-server = "https://github.com/msteen/nixos-vscode-server/tarball/master"}/modules/vscode-server/home.nix";
  };
  outputs = inputs@{ self, nixpkgs, nixvim, vscode-server, ... }:
    let
      system = "aarch64-linux";
      myPkgs = import nixpkgs {
        inherit system;
        #config =
        config = {
          allowUnfree = true;
          # allowBroken = true;
          allowUnsupportedSystem = true;
        };
      };

    in {
      homeConfigurations = {
        cosmos = inputs.home-manager.lib.homeManagerConfiguration {
          pkgs = myPkgs; # nixpkgs.legacyPackages.${system};
          modules = [
            {
              # with pkgs; [zed-editor];
              programs.htop.enable = true;
              # programs.emacs.enable = true;
            }
            ({ pkgs, ... }: {
              home.packages = [
                pkgs.zed-editor
                pkgs.silver-searcher
                pkgs.neovide
                # inputs.nixvim.homeManagerModules.nixvim
              ];
            })
            /* vscode-server.nixosModules.default
                 ({ config, pkgs, ... }: {
               services.vscode-server.enable = true;
               })
            */
            #                         vscode-server.nixosModule
            #                           ({ config, pkgs, ... }: {
            #                                  services.vscode-server.enable = true;})
            #./yi
            #		./brickFileTree
            # ./taskell
            #            ./helix
            #            ./misc
            # ./bash
            #./zsh
            ./tmux
            # ./tree
            ./kitty
            nixvim.homeManagerModules.nixvim
            #./code
            #            ./kakoune
            #            ./golang
            ./nixvim
            # ./neovim
            #            ./fish
            #            ./starship
            ./direnv
            ./git
            ./emacs
            ./alacritty
            #            ./hledger
            #	    ./zoxide
            # ./texlive
            #./pdflatex
            #            ./pandoc
            #            ./lazygit
            {

              nixpkgs.overlays = with inputs;
                [
                  emacs-overlay.overlay
                  # nixvim.overlay
                  #  nur.overlay
                  # nixvim.overlay
                  #  kmonad.overlay
                  #neovim-nightly-overlay.overlay
                ];

              programs.home-manager.enable = true;
              home = {
                username = "vamshi";
                homeDirectory = "/home/vamshi";
                stateVersion = "23.11";
              };
            }

          ];

          #};
        };
      };
    };
}
