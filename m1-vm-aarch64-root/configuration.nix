# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.

    ./hardware-configuration.nix

    (fetchTarball
      "https://github.com/msteen/nixos-vscode-server/tarball/master")

  ];

#  services.neo4j = {
#    enable = true;

  #};
 # database
services.postgresql = {
    enable = true;
   #package = pkgs.postgresql_10;
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      local all all trust
      host all all 127.0.0.1/32 trust
      host all all ::1/128 trust
    '';
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE ROLE nixcloud WITH LOGIN PASSWORD 'nixcloud' CREATEDB;
      CREATE DATABASE nixcloud;
      GRANT ALL PRIVILEGES ON DATABASE nixcloud TO nixcloud;
    '';

#    package = pkgs.postgresql_12;
};


  nix = {

    #
    #package  = pkgs.nixUnstable;
    #binaryCachePublicKeys = [ 
    settings = {
      #      experimental-features = ["nix-command" "flakes"];
      #     trusted-users = [ "root" "vamshi" ];
      auto-optimise-store = true;

      # Binary Cache for Haskell.nix     
      trusted-public-keys = [
        "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
        #        "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
      ];
      substituters = [
        "https://cache.iog.io"
        #          "https://nixcache.reflex-frp.org" 
      ];
    };
    gc = {
      automatic = true;
      dates = "daily";
    };
    extraOptions =
      "	experimental-features = nix-command flakes\n	keep-outputs = true\n	keep-derivations = true\n";
  };

  #nix.binaryCaches = [ "https://nixcache.reflex-frp.org" ];
  #nix.binaryCachePublicKeys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];

  services.vscode-server.enable = true;

  #nix.settings = {
  #  substituters = [
  #};

  # virtualisation docker
#  virtualisation.docker.enable = true;

  environment.variables = {
    LIBGL_ALWAYS_SOFTWARE = "1"; # vm (utm)
    GDK_SCALE = "1";
    GDK_DPI_SCALE = "0.4";
    _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
  };

  security.sudo.wheelNeedsPassword = false;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "cosmos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  fonts.fonts = with pkgs; [
    fira-code
    roboto-mono
    inconsolata-nerdfont
    (nerdfonts.override {
      fonts = [ "RobotoMono" ];
    }) # "CascadiaCode" "Hasklig" "JetBrainsMono" "Mononoki" "SourceCodePro" ]; })
  ];

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    dpi = 180;
  };
  services.xserver.windowManager.xmonad = {
    enable = true;
    #services.xserver.windowManager.xmonad.
    enableContribAndExtras = true;
    config = ./xmonad.hs;
  };
  services.xserver.displayManager =  {

#sessionCommands = ''
 #  ${pkgs.xorg.xrandr}/bin/xrandr --newmode 1080x1920_60.00  176.50  1080 1168 1280 1480  1920 1923 1933 1989 -hsync +vsync ;
 #  xrandr --addmode Virtual-1 1080x1920_60.00 ;
 #  xrandr --output Virtual-1 --mode 1080x1920_60.00
#                '';
#         ${pkgs.xorg.xrandr}/bin/xrandr -s '1920x1080'
        defaultSession = "none+xmonad";
        autoLogin = {
          enable = true;
          user = "vamshi";
        };
	};
  #  services.xserver.videoDrivers = ["qxl"];
  #  services.xserver.windowMana

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
    #   packages = with pkgs; [
    #     firefox
    #     thunderbird
    #   ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #   wget
    sqlite
    firefox
    nixfmt
    cachix
    glibc
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

