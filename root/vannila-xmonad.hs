import XMonad
import XMonad.Util.EZConfig (additionalKeysP)

import XMonad.Hooks.SetWMName

import XMonad.Util.SpawnOnce


main = xmonad $ def {
   modMask = mod4Mask
   , terminal = "alacritty"
   ,startupHook        = myStartupHook

  } `additionalKeysP` myKeys

myStartupHook :: X ()
myStartupHook = do
--	spawnOnce "xrandr --newmode 1080x1920_60.00  176.50  1080 1168 1280 1480  1920 1923 1933 1989 -hsync +vsync ; xrandr --addmode Virtual-1 1080x1920_60.00 ; xrandr --output Virtual-1 --mode 1080x1920_60.00"
	spawnOnce "xrandr --newmode \"2560x1440_60.00\"  312.25  2560 2752 3024 3488  1440 1443 1448 1493 -hsync +vsync ; xrandr --addmode Virtual-1 \"2560x1440_60.00\" ; xrandr -s  \"2560x1440_60.00\""
--        spawnOnce "xrandr -s 1920x1080"
	setWMName "LG3D"

myKeys = [
  ("M-e" , spawn "emacsclient -c -a emacs")
  ]
