{ lib, pkgs, ... }: {
  imports = [
    ./window.nix
    ./options.nix 
    ./doom.nix ];
  programs.emacs.enable = true;
  my-config.vemacs.enable= true;
  my-config.kakoune.enable= true;
  my-config.emux-keybindings.enable= true;
  my-config.modus-theme.enable = true;
  my-config.window.enable= true;
  my-config.history.enable= true;
  my-config.prot-experiment.enable= true;
  # my-config.knuth.enable= true;
  # my-config.doom.enable = true;
}
#   programs.emacs = {
#     enable = true;
# package = pkgs.emacs.override {
#   withTreeSitter = true;
# };
# services.emacs = {
# enable = true;
# client = {
# enable = true;
# arguments = [ "-c" ];
# };
# defaultEditor = true;
# socketActivation.enable = true;
# };
