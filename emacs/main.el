(load-theme 'modus-vivendi t)

(global-set-key (kbd "C-x C-m") 'execute-extended-command)

(set-face-attribute 'default nil
                    ;; :family "Roboto Mono"
                    :weight 'normal
                    :height 65)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

 (make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

 (setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
	auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

 (setq create-lockfiles nil)

(add-hook 'dired-mode-hook 'dired-hide-details-mode)

(setq evil-want-integration t)
(setq evil-want-keybinding nil)
(require 'evil)
(when (require 'evil-collection nil t)
  (evil-collection-init))
(evil-mode)

;; (evil-set-leader 'normal (kbd ";"))
 (defvar my-leader-map (make-sparse-keymap)
 "")
 (define-key evil-normal-state-map ";" my-leader-map)
 (define-key my-leader-map "w" 'save-buffer)
