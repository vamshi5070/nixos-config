(global-unset-key (kbd "C-z"))

(defun magit-tab-new()
  (interactive)
  (tab-new)
  (magit-status)
  (delete-other-windows)
  )
(global-set-key (kbd "C-x t g") 'magit-tab-new)

;; (load-theme 'modus-vivendi t)

(global-set-key (kbd "C-x C-m") 'execute-extended-command)

(set-face-attribute 'default nil
                    ;; :family "Roboto Mono"
                    :weight 'normal
                    :height 100)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

 (make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

 (setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
	auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

 (setq create-lockfiles nil)

(defun open-emacs-file()
    (interactive)
    (find-file "/home/vamshi/nixos-config/emacs/vemacs.org")
    )

(add-hook 'org-mode-hook #'org-shifttab)

;; (require 'emux-keybindings)

(add-hook 'dired-mode-hook 'dired-hide-details-mode)
	(put 'dired-find-alternate-file 'disabled nil)
;; (define-key dired-mode-map (kbd "j") 'dired-next-line)
;;        (define-key dired-mode-map (kbd "k") 'dired-previous-line)
;;        (define-key dired-mode-map (kbd "h") 'dired-up-directory)
;;        (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
;; 	 (define-key dired-mode-map (kbd "/") 'dired-goto-file)

(defun get-ls-output()
  (interactive)
  (shell-command "ls" "*output.txt*")
  )

(defun get-home-up-output()
  (interactive)
  (shell-command  "home-manager switch --flake /home/vamshi/nixos-config#cosmos" "*output.org*")
  )
