{ config, lib, pkgs, ... }:
with lib; {
  options = {
    my-config.window.enable = mkOption {
      type = types.bool;
      default = false;
    };
  };
  config = mkIf config.my-config.window.enable { 
      programs.emacs.extraConfig = ''
  (defun emux/horizontal-split ()
    (interactive )
    (split-window-below)
    (other-window 1))
    
(defun emux/vertical-split ()
   (interactive )
   (split-window-right)
   (other-window 1))

(defun emux/up-window ()
   (interactive )
   (other-window -1))
(defun emux/down-window ()
   (interactive )
   (other-window 1))
'';
  };
}
