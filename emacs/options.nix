{ config, lib, pkgs, ... }:
with lib; {
  options = {
    my-config.knuth.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.history.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.vemacs.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.modus-theme.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.emux-keybindings.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.kakoune.enable = mkOption {
      type = types.bool;
      default = false;
    };
    my-config.prot-experiment.enable = mkOption {
      type = types.bool;
      default = false;
    };
  };
  config = mkMerge [
    (mkIf config.my-config.vemacs.enable {
      programs.emacs = {
        extraPackages = epkgs: with epkgs; [nix-mode tuareg magit];
        extraConfig = ''
          (org-babel-load-file
           (expand-file-name
            "~/nixos-config/emacs/vemacs.org"
            )
            )
        '';
      };
      home.file.".emacs.d/early-init.el".source = ./early-init.el;
    })
    (mkIf config.my-config.prot-experiment.enable {
      programs.emacs = {
        extraConfig = ''
        (spacious-padding-mode 1)
'';     
        extraPackages = epkgs: with epkgs; [spacious-padding standard-themes dired-preview ];
      };
    })
    (mkIf config.my-config.modus-theme.enable {
      programs.emacs.extraConfig = ''
      (load-theme 'modus-vivendi t)
      '';
    })
    (mkIf config.my-config.kakoune.enable {
      programs.emacs.extraConfig = (builtins.readFile ./kakoune.el);
    })
    (mkIf config.my-config.emux-keybindings.enable {
      programs.emacs.extraConfig = (builtins.readFile ./emux-keybindings.el);
    })
    
    (mkIf config.my-config.history.enable {
      programs.emacs.extraConfig = ''
 ;; Save history                 
     (savehist-mode t)               
 ;;  saveplace   
 (save-place-mode 1)                           ;; Remember point in files
 (setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
	(replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				  save-place-ignore-files-regexp t t))

 (recentf-mode 1)
 (setq recentf-max-menu-items 25)
 (setq recentf-max-saved-items 25)
 (global-set-key "\C-x\ \C-r" 'recentf-open-files)
      '';
    })
  ];
}
