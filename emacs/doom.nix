{ config, lib, pkgs, ... }:
with lib; {
  options = {
    my-config.doom.enable = mkOption {
      type = types.bool;
      default = false;
    };
  };
  config =
    mkIf config.my-config.doom.enable { home.packages = with pkgs; [ fd ripgrep]; };
}
# in
# {
#   programs.emacs.enable = true;
# }
#   programs.emacs = {
#     enable = true;
# package = pkgs.emacs.override {
#   withTreeSitter = true;
# };
# comment for intial config,

# uncomment for intial config,
/* extraConfig = ''
     (org-babel-load-file
      (expand-file-name
       "~/nixos-config/emacs/empty.org"
       )
       )
   '';
*/

# services.emacs = {
# enable = true;
# client = {
# enable = true;
# arguments = [ "-c" ];
# };
# defaultEditor = true;
# socketActivation.enable = true;
# };

# home.file.".emacs.d/init.el".source = ./init.el;
# }

#  extraPackages = epkgs: with epkgs; [
#    nix-mode
#    evil
#    evil-collection
#    general
#    magit
#    tuareg
#    rust-mode
#  ];
# };
#
