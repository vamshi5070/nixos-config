(savehist-mode t)
(save-place-mode t)
(setq save-place-ignore-files-regexp
       (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				 save-place-ignore-files-regexp t t))

(defun normal-font ()
(interactive)
(set-face-attribute 'default nil
                    :family "Roboto Mono"
                    :weight 'light
                    :height 45)

(set-face-attribute 'bold nil
                    :family "Roboto Mono"
                    :weight 'regular)

(set-face-attribute 'italic nil
                    :family "Victor Mono"
                    :weight 'semilight
                    :slant 'italic)

(set-fontset-font t 'unicode
                    (font-spec :name "Inconsolata Light"
                               :size 16) nil)

(set-fontset-font t '(#xe000 . #xffdd)
                     (font-spec :name "RobotoMono Nerd Font"
                                :size 12) nil)

)
(defun big-font ()
(interactive)
(set-face-attribute 'default nil
                    :family "Roboto Mono"
                    :weight 'light
                    :height 75)

(set-face-attribute 'bold nil
                    :family "Roboto Mono"
                    :weight 'regular)

(set-face-attribute 'italic nil
                    :family "Victor Mono"
                    :weight 'semilight
                    :slant 'italic)

(set-fontset-font t 'unicode
                    (font-spec :name "Inconsolata Light"
                               :size 16) nil)

(set-fontset-font t '(#xe000 . #xffdd)
                     (font-spec :name "RobotoMono Nerd Font"
                                :size 12) nil)

)
(normal-font)

;;  (load-theme 'modus-vivendi t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(use-package attrap
  :straight t)

;; Enable Haskell Mode
(use-package haskell-mode
  :straight t :ensure t
  :hook (haskell-mode . interactive-haskell-mode))

;; Enable Intero
(use-package dante
  :straight t :ensure t
  :hook (haskell-mode . dante-mode))

;; Enable Haskell Docs
(use-package dash-at-point
  :straight t :ensure t
  :bind (("C-c C-d" . dash-at-point)))

;; Enable Flycheck
;; (use-package flycheck-haskell
  ;; :straight t :ensure t
  ;; :config
  ;; (add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))

;; ;; Enable Company Mode
;; (
 ;; use-package company-ghc
  ;; :straight t :ensure t
  ;; :config
  ;; (add-to-list 'company-backends 'company-ghc))

;; Enable Yasnippet
(use-package yasnippet
  :straight t :ensure t
  :hook (haskell-mode . yas-minor-mode))

;; Enable Rainbow Delimiters
(use-package rainbow-delimiters
  :straight t :ensure t
  :hook (haskell-mode . rainbow-delimiters-mode))

;; Enable HIndent
(use-package hindent
  :straight t :ensure t
  :hook (haskell-mode . hindent-mode))

;; Enable Show Paren Mode
(show-paren-mode 1)

;; Enable Electric Pair Mode
(electric-pair-mode 1)

;; Enable Paredit Mode
(use-package paredit
  :straight t :ensure t
  :hook (haskell-mode . paredit-mode))

;; Enable Smartparens Mode
(use-package smartparens
  :straight t :ensure t
  :hook (haskell-mode . smartparens-mode))

;; Enable Highlight Indentation Mode
(use-package highlight-indentation
  :straight t :ensure t
  :hook (haskell-mode . highlight-indentation-mode))

;; Enable Indent Guides Mode
(use-package indent-guide
  :straight t :ensure t
  :hook (haskell-mode . indent-guide-mode))

;; Enable HLint Mode
;;(use-package hlint-refactor
;;  :ensure t
;;  :bind (

(use-package nix-mode
  :straight t)

(use-package evil
  :straight t
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :straight t
  :after evil
  :ensure t
  :config
  (evil-collection-init))
(evil-global-set-key 'normal (kbd "SPC b b") 'consult-buffer)
(evil-global-set-key 'normal (kbd "SPC s s") 'consult-line)
(evil-global-set-key 'normal (kbd "SPC f s") 'save-buffer)
(evil-global-set-key 'normal (kbd "SPC f f") 'find-file)
(evil-global-set-key 'normal  (kbd "SPC SPC") 'execute-extended-command)
(evil-global-set-key 'visual (kbd "SPC SPC") 'execute-extended-command)

(with-eval-after-load 'dired
  ;;(define-key dired-mode-map (kbd "M-p") 'peep-dired)
  (evil-define-key 'normal dired-mode-map (kbd "h") 'dired-up-directory)
  (evil-define-key 'normal dired-mode-map (kbd "l") 'dired-find-file)) ; use dired-find-file instead if not using dired-open package
;;  (evil-define-key 'normal peep-dired-mode-map (kbd "j") 'peep-dired-next-file)
;;  (evil-define-key 'normal peep-dired-mode-map (kbd "k") 'peep-dired-prev-file))

(straight-use-package '(nano-theme :type git :host github
                                   :repo "rougier/nano-theme"))

;; SVG tags, progress bars & icons
(straight-use-package
 '(svg-lib :type git :host github :repo "rougier/svg-lib"))

;; Replace keywords with SVG tags
(straight-use-package
 '(svg-tag-mode :type git :host github :repo "rougier/svg-tag-mode"))

(require 'svg-lib)
(require 'svg-tag-mode)

;; NANO modeline
(straight-use-package
 '(nano-modeline :type git :host github :repo "rougier/nano-modeline"))

;; NANO header
(straight-use-package
 '(minibuffer-header :type git :host github :repo "rougier/minibuffer-header"))
(require 'nano-theme)
(nano-dark)
(nano-modeline-mode)

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		   prog-mode-hook
		   dired-mode-hook

		   ;; term-mode-hook
		   ;; vterm-mode-hook
		   ;; shell-mode-hook
		   ;; dired-mode-hook
		   ;; eshell-mode-hook
		   ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq 
 ;; org-ellipsis "▼  " ;;⤵ 
 ;; org-startup-indented t
 org-src-tab-acts-natively t
 org-hide-emphasis-markers t
 org-fontify-done-headline t
 org-hide-leading-stars t
 org-pretty-entities t
 org-odd-levels-only t
 ) 
;;  (add-hook 'org-mode-hook #'org-shifttab)
;; (add-hook 'org-mode-hook #'variable-pitch-mode)
;; (setq org-bullets-bullet-list '(
;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "
(setq org-src-fontify-natively t
      org-startup-folded t
      org-edit-src-content-indentation 0)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))
(setq create-lockfiles nil)

(use-package envrc
  :straight t)
(envrc-global-mode t)

(straight-use-package '( vertico :files (:defaults "extensions/*")
             :includes (vertico-buffer
                    vertico-directory
                    vertico-flat
                    vertico-indexed
                    vertico-mouse
                    vertico-quick
                    vertico-repeat
                    vertico-reverse)))
  ;; (straight-use-package 'vertico)
  (straight-use-package 'orderless)
    ;; (straight-use-package 'vertico-directory)
  (straight-use-package 'consult)
(setq consult-preview-key nil) ; No live preview
    (vertico-mode t)
    (vertico-indexed-mode t)
           (setq vertico-count 17
           vertico-resize nil
             vertico-cycle t
             vertico-count-format nil ; No prefix with number of entries
             completion-in-region-function
       (lambda (&rest args)
         (apply (if vertico-mode
                #'consult-completion-in-region
              #'completion--in-region)
            args)))

       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
       (define-key vertico-map (kbd "C-j") #'vertico-next)
       (define-key vertico-map (kbd "C-k") #'vertico-previous)
       (define-key vertico-map (kbd "C-'") #'vertico-quick-insert)
       (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
     ;; (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)
  ;; (require 'vertico-directory)

    ;; Configure directory extension.
    (use-package vertico-directory
  :straight nil
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
          ("RET" . vertico-directory-enter)
          ("DEL" . vertico-directory-delete-char)
          ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
      (setq completion-styles '(orderless)
            completion-category-defaults nil
            completion-category-overrides '((file (styles partial-completion))))

       (global-set-key (kbd "C-x b") 'consult-buffer)
       (global-set-key (kbd "C-x C-b") 'consult-buffer)
    ;; (vertico-posframe-mode 1)
    ;; (mini-frame-mode 1)	;; 
    ;; (minibuffer-header-mode 1)
      ;; (require 'io-bling)
     ;; (require 'emux-minibuffer)


    ;; (load-library 'ob-haskell nil)

(setq vertico-grid-separator
      #("  |  " 2 3 (display (space :width (1))
                             face (:background "#ECEFF1")))

      vertico-group-format
      (concat #(" " 0 1 (face vertico-group-title))
              #(" " 0 1 (face vertico-group-separator))
              #(" %s " 0 4 (face vertico-group-title))
              #(" " 0 1 (face vertico-group-separator
                          display (space :align-to (- right (-1 . right-margin) (- +1)))))))

(set-face-attribute 'vertico-group-separator nil
                    :strike-through t)
(set-face-attribute 'vertico-current nil
                    :inherit '(nano-strong nano-subtle))
(set-face-attribute 'completions-first-difference nil
                    :inherit '(nano-default))

(defun minibuffer-format-candidate (orig cand prefix suffix index _start)
  (let ((prefix (if (= vertico--index index)
                    "  "
                  "   "))) 
    (funcall orig cand prefix suffix index _start)))

(advice-add #'vertico--format-candidate
           :around #'minibuffer-format-candidate)

(defun vertico--prompt-selection ()
  "Highlight the prompt"

  (let ((inhibit-modification-hooks t))
    (set-text-properties (minibuffer-prompt-end) (point-max)
                         '(face (nano-strong nano-salient)))))
 
(defun minibuffer-vertico-setup ()

  (setq truncate-lines t)
  (setq completion-in-region-function
        (if vertico-mode
            #'consult-completion-in-region
          #'completion--in-region)))

(add-hook 'vertico-mode-hook #'minibuffer-vertico-setup)
(add-hook 'minibuffer-setup-hook #'minibuffer-vertico-setup)

(require 'all-the-icons)

(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)
(setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
	      dired-recursive-deletes 'top  ;; Always ask recursive delete
	      dired-dwim-target t	    ;; Copy in split mode with p
	      dired-auto-revert-buffer t
	      dired-listing-switches "-alh -agho --group-directories-first"
	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
	      dired-isearch-filenames 'dwim ;;)
	      dired-omit-files "^\\.[^.].*"
	      dired-omit-verbose nil
	      dired-hide-details-hide-symlink-targets nil
	      delete-by-moving-to-trash t
	      )

(straight-use-package 'pulsar)
(customize-set-variable
 'pulsar-pulse-functions
 '(other-window
   xah-next-window-or-frame)
 )
(setq pulsar-face 'pulsar-magenta)
(setq pulsar-delay 0.055)
;; (define-key global-map (kbd  
(pulsar-global-mode t)
(global-hl-line-mode t)

(use-package svelte-mode
  :straight t)
