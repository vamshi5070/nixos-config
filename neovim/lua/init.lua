require("toggleterm").setup({
	size=20,
	open_mapping = [[<c-\>]],
})
-- Lua
require('catppuccin').load()
--require("telescope").setup({
--
--
--
--
--require("catppuccin").setup({})
--require('go').setup()
--require('nvim-treesitter.configs').setup({
--        highlight = {
--                enable = true,
--        },
--})

require("indent_blankline").setup {
    -- for example, context is off by default, use this to turn it on
    show_current_context = true,
    show_current_context_start = true,
}

local options = {
	expandtab = true,
        relativenumber = true,
        wrap = false,
        syntax = on,
	clipboard = "unnamedplus",
        cmdheight= 1,
        guifont= "Iosevka:h7",
        termguicolors = true
 --       colorscheme = codedark
}

for key,value in pairs(options) do
	vim.opt[key] = value
end

-- OR setup with some options
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    width = 30,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})
