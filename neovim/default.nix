{ pkgs, ... }: {
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    #package = pkgs.neovim-nightly;
    plugins = with pkgs.vimPlugins; [
      toggleterm-nvim
      nvim-tree-lua
      nvim-treesitter
      telescope-nvim
      indent-blankline-nvim
      # themes
      catppuccin-nvim
      onedark-nvim
     # haskell-tools-nvim
     # nvim-treesitter-parsers.haskell
    ];
    extraConfig = ''
      luafile ~/nixos-config/neovim/lua/init.lua
      luafile ~/nixos-config/neovim/lua/keymaps.lua
    '';
  };
}

