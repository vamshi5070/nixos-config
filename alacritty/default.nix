_: {
  programs.alacritty = {
    enable = true;
    settings = {
      #  font = "Iosevka Term SS05 12";
      font = {
        normal = {
          family = "Monospace";
        };
        size = 12.0;
      };
    };
  };
}
